import { Injectable } from '@angular/core';
import { Produto } from './produto.model';
import  {HttpClient}  from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  formData  : Produto;
  list : Produto[];
  readonly  rootURL="http://localhost:50811/api/usuario"

  constructor(private http  : HttpClient) { }

  postProduto(formData : Produto){
    return this.http.post(this.rootURL+ '/CadastrarProduto', formData);
  }

  refreshList(){
    this.http.get(this.rootURL+ '/ConsultarProdutos')
    .toPromise().then(res => this.list = res as Produto[]);
  }

  putProduto(formData : Produto){
    return this.http.put(this.rootURL+ '/AlterarProduto' ,formData);
  }

  deleteProduto(id  : number)
  {
    return this.http.delete(this.rootURL+ '/ExcluirProduto/' + id)
  }
}
