import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/shared/produto.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

  constructor(private service : ProdutoService,
    private toastr  : ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? :  NgForm){
    if(form != null)
    form.resetForm();   
    this.service.formData = {
      Id  : null,
      Nome : '',
      Descricao : '',
      Preco : null
    }
  }

  onSubmit(form : NgForm){
    if(form.value.Id == null)
    this.insertRecord(form);
    else
    this.UpdateRecord(form);
  }

  insertRecord(form : NgForm){
    this.service.postProduto(form.value).subscribe(res => {
      this.toastr.success('Inserido com sucesso', 'Produto.Registrar')
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  UpdateRecord(form : NgForm){
    this.service.putProduto(form.value).subscribe(res => {
      this.toastr.info('Alterado com sucesso', 'Produto.Registrar')
      this.resetForm(form);
      this.service.refreshList();
    });
  }
}
