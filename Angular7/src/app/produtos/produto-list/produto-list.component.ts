import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/shared/produto.service';
import { Produto } from 'src/app/shared/produto.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-produto-list',
  templateUrl: './produto-list.component.html',
  styleUrls: ['./produto-list.component.css']
})
export class ProdutoListComponent implements OnInit {

  constructor(private service : ProdutoService,
    private toastr  : ToastrService) { }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(prod : Produto){
    this.service.formData = Object.assign({},prod);
  }

  onDelete(id : number){
    if(confirm('Deseja deletar esse registro?')){
    
    this.service.deleteProduto(id).subscribe(res =>{
      this.service.refreshList();
      this.toastr.warning('Deletado com sucesso', 'Produto.Registrar')
    });
  }
}

}
