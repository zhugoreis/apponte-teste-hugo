﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apponte.Models
{
    public class ServicosDeBelezaModel
    {
        private int id;
        private string nome;
        private string descricao;
        private double preco;

        public ServicosDeBelezaModel()
        {
        }

        public ServicosDeBelezaModel(int id, string nome, string descricao, double preco)
        {
            this.Id = id;
            this.Nome = nome;
            this.Descricao = descricao;
            this.Preco = preco;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }

        }

        public string Nome
        {
            get
            {
                return nome;
            }

            set
            {
                nome = value;
            }
        }

        public string Descricao
        {
            get
            {
                return descricao;
            }

            set
            {
                descricao = value;
            }
        }

        public double Preco
        {
            get
            {
                return preco;
            }

            set
            {
                preco = value;
            }
        }
    }
}