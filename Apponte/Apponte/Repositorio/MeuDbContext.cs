﻿using Apponte.Entidade;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Apponte.Repositorio
{
    public class MeuDbContext : DbContext
    {
        public virtual DbSet<ServicoDeBeleza> ServicoDeBeleza { get; set; }


        public MeuDbContext()
           : base("produto")
        {
            Database.SetInitializer<MeuDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new ServicoDeBelezaMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}