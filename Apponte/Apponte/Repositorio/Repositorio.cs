﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apponte.Repositorio
{
    public class Repositorio
    {
        private readonly MeuDbContext _contexto;
        public Repositorio(object contexto)
        {
            _contexto = (MeuDbContext)contexto;
        }

        public Repositorio()
        {
            _contexto = new MeuDbContext();
        }
    }
}