﻿using Apponte.Entidade;
using System.Data.Entity.ModelConfiguration;

namespace Apponte.Repositorio
{
    public class ServicoDeBelezaMap : EntityTypeConfiguration<ServicoDeBeleza>
    {
        public ServicoDeBelezaMap()
        {
            ToTable("produto");
            HasKey(t => t.Id);
        }
    }
}