﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apponte.Entidade
{
    public class ServicoDeBeleza
    {
        public int Id { get; set; }
        public String Nome { get; set; }
        public String Descricao { get; set; }
        public double Preco { get; set; }
    }
}