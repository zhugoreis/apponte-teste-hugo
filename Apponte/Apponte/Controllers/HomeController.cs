﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Apponte.Entidade;
using Apponte.Models;
using Apponte.Repositorio;
using MySql.Data.MySqlClient;

namespace Apponte.Controllers
{
    
    [RoutePrefix("api/usuario")]
    public class HomeController : ApiController
    {
        private static List<ServicosDeBelezaModel> listaProdutos = new List<ServicosDeBelezaModel>();

        [AcceptVerbs("POST")]
        [Route("CadastrarProduto")]
        public string CadastrarProduto(ServicosDeBelezaModel produto)
        {
            ServicoDeBeleza servicoDeBeleza = new ServicoDeBeleza
            {
                Id = produto.Id,
                Nome = produto.Nome,
                Descricao = produto.Descricao,
                Preco = produto.Preco
            };

            try
            {
                using (var ctx = new MeuDbContext())
                {
                    ctx.ServicoDeBeleza.Add(servicoDeBeleza);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return $"Erro ao Salvar Produto {ex.Message}";
            }

            return "Produto cadastrado com sucesso!";
        }

        [AcceptVerbs("PUT")]
        [Route("AlterarProduto")]
        public string AlterarProduto(ServicosDeBelezaModel produto)
        {
            try
            {
                using (var ctx = new MeuDbContext())
                {
                    var servicoDeBeleza = ctx.ServicoDeBeleza.FirstOrDefault(t => t.Id == produto.Id);
                    if (servicoDeBeleza == null)
                    {
                        return $"Servico de Beleza {produto.Nome} nao encontrado";
                    }

                    if(servicoDeBeleza.Preco != produto.Preco){servicoDeBeleza.Preco = produto.Preco;}
                    if (servicoDeBeleza.Nome != produto.Nome){servicoDeBeleza.Nome = produto.Nome;}
                    if (servicoDeBeleza.Descricao != produto.Descricao){servicoDeBeleza.Descricao = produto.Descricao;}
                   
                    ctx.Entry(servicoDeBeleza).State = EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return $"Erro ao Alterar o Produto {ex.Message}";
            }

            return "Produto alterado com sucesso!";
        }

        [AcceptVerbs("DELETE")]
        [Route("ExcluirProduto/{id}")]
        public string ExcluirProduto(int id)
        {

            try
            {
                using (var ctx = new MeuDbContext())
                {
                    var servicoDeBeleza = ctx.ServicoDeBeleza.FirstOrDefault(t => t.Id == id);
                    if (servicoDeBeleza == null)
                    {
                        return $"Servico de Beleza ID {id} nao encontrado";
                    }
                    ctx.Entry(servicoDeBeleza).State = EntityState.Deleted;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return $"Erro ao excluir o Produto {ex.Message}";
            }

            return "Produto excluido com sucesso!";
        }

        [AcceptVerbs("GET")]
        [Route("ConsultarProdutoPorId/{id}")]
        public ServicosDeBelezaModel ConsultarProdutoPorCodigo(int id)
        {

            ServicosDeBelezaModel produto = listaProdutos.Where(n => n.Id == id)
                                                .Select(n => n)
                                                .FirstOrDefault();

            return produto;
        }

        [AcceptVerbs("GET")]
        [Route("ConsultarProdutos")]
        public List<ServicosDeBelezaModel> ConsultarProdutos()
        {
            try
            {
                using (var ctx = new MeuDbContext())
                {
                    var retorno = ctx.ServicoDeBeleza.Select(t => new ServicosDeBelezaModel
                    {
                        Id = t.Id,
                        Nome = t.Nome,
                        Descricao = t.Descricao,
                        Preco = t.Preco
                    }).ToList();
                    return retorno;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
